#ifndef _PLG_MANAGER_HPP_
#define _PLG_MANAGER_HPP_

#include <cassert>
#include <map>
#include <memory>
#include <dlfcn.h>

#include "ipluginfactory.hpp"

class PluginManager
{
public:
    using PluginMap = std::map<std::string, Plugin>;
              
    // Plugins database
    PluginMap m_plugindb;

    PluginManager()
    {                                                                                                               
              
    }

    std::string GetExtension() const
    {
        std::string ext;
        ext = ".so";  // Linux, BDS, Solaris and so on.
        return ext;
    }

    IPluginFactory* addPlugin(const std::string& name)
    {                                        
        std::string fileName = name + GetExtension();
  
        m_plugindb[name] = Plugin(fileName);
        return m_plugindb[name].GetInfo();

    }

    IPluginFactory* GetPluginFactory(const char* pluginName)
    {
        auto it = m_plugindb.find(pluginName);
        if(it == m_plugindb.end())
            return nullptr;
            
        return it->second.GetInfo();

    }

    /** @brief Instantiate some class exported by some plugin.
    *  @details
    *  This member function returns a pointer to the object
    *  casted as void pointer.  It is responsibility of the caller to
    *  cast the return pointer to a proper interface implemented by
    *  the loaded class.
    **/                    

    void* CreateInstance(std::string pluginName, std::string className)
    {
        auto it = m_plugindb.find(pluginName);
        if(it == m_plugindb.end())
            return nullptr;                

        return it->second.CreateInstance(className);
    }

 

    /** @brief Instantiate a class exported by some loaded plugin.
    *  @tparam T           Interface (interface class) implemented by the loaded class.
    *  @param  pluginName  Name of the plugin that exports the class.
    *  @param  className   Name of the class exported by the plugin.
    *  @return             Instance of exported class casted to some interface T.
    * */
    template<typename T>
    std::shared_ptr<T>
    CreateInstanceAs(std::string pluginName, std::string className)
    {
        void* pObj = CreateInstance(std::move(pluginName), std::move(className));
        return std::shared_ptr<T>(reinterpret_cast<T*>(pObj));
    }

}; /* --- End of class PluginManager --- */

#endif
