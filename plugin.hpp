#ifndef _PLUGIN_HPP_
#define _PLUGIN_HPP_

#include <cassert>
#include <map>
#include <memory>
#include <dlfcn.h>

#include "ipluginfactory.hpp"

using namespace std;

/** @brief Class form managing and encapsulating shared libraries loading  */
class Plugin
{            
public:
    /** @brief Function pointer to DLL entry-point */
    using GetPluginInfo_fp = IPluginFactory* (*) ();

    /** @brief Name of DLL entry point that a Plugin should export */
    static constexpr const char* DLLEntryPointName = "GetPluginFactory";

    /** @brief Shared library handle */
    void * m_hnd = nullptr;

    /** @brief Shared library file name */
    std::string  m_file = "";

    /** @brief Flag to indicate whether plugin (shared library) is loaded into current process. */
    bool m_isLoaded = false;

    /** @brief Pointer to shared library factory class returned by the DLL entry-point function */
    IPluginFactory * m_info  = nullptr;

    Plugin()
    {

    }

    explicit Plugin(std::string file)
    {
        m_file = std::move(file);
        m_hnd  = (void*) dlopen(m_file.c_str() , RTLD_LAZY);

        m_isLoaded = true;
        assert(m_hnd != nullptr);

        auto dllEntryPoint = reinterpret_cast<GetPluginInfo_fp>(dlsym(m_hnd, DLLEntryPointName));
        assert(dllEntryPoint != nullptr);

        // Retrieve plugin metadata from DLL entry-point function
        m_info = dllEntryPoint();

    }

    ~Plugin()
    {
        this->Unload();
    }

    Plugin(Plugin const&) = delete;
    Plugin& operator=(const Plugin&) = delete;

    Plugin(Plugin&& rhs)
    {
        m_isLoaded      = std::move(rhs.m_isLoaded);
        m_hnd               = std::move(rhs.m_hnd);
        m_file                 = std::move(rhs.m_file);
        m_info      = std::move(rhs.m_info);

    }

    Plugin& operator=(Plugin&& rhs)
    {
        std::swap(rhs.m_isLoaded, m_isLoaded);
        std::swap(rhs.m_hnd,  m_hnd);
        std::swap(rhs.m_file, m_file);
        std::swap(rhs.m_info, m_info);

        return *this;
    }

    IPluginFactory* GetInfo() const
    {
        return m_info;
    }

    void* CreateInstance(std::string const& className)
    {
        return m_info->Factory(className.c_str());
    }

    bool isLoaded() const
    {
        return m_isLoaded;
    }

    void Unload()
    {
        if(m_hnd != nullptr) 
        {
            ::dlclose(m_hnd);
            m_hnd = nullptr;
            m_isLoaded = false;
        }

    }

};

#endif