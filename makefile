CPPFLAGS_SO=g++ -m64 -Wall -lstdc++ -std=c++1y  -pthread -g -O -shared -fPIC
CPPFLAGS=g++ -m64 -Wall -ldl -lstdc++ -std=c++1y  -pthread 
 

all: PluginA.so main

PluginA.so: PluginA.cpp ipluginfactory.hpp
	$(CPPFLAGS_SO) PluginA.cpp -o PluginA.so

main : main.o
	$(CPPFLAGS) -o main main.o

main.o : main.cpp
	$(CPPFLAGS) -c main.cpp

clean:
		rm -rf *.so
		rm -rf *.o
		rm main